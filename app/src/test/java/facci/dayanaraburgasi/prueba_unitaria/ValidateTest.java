package facci.dayanaraburgasi.prueba_unitaria;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ValidateTest {
    @Test
    public void isEmptyEditText_ReturnsTrue() {


        assertTrue(Validate.validarCampoVacio(""));
    }

    @Test
    public void isEmptyEditText_ReturnsFalse() {


        assertFalse(Validate.validarCampoVacio("dflksdfkdf"));
    }

    @Test
    public void isEmailFormatCorrect_ReturnsFalse() {

        assertFalse(Validate.validarCorreo("dayanara64gmail.com"));
    }

    @Test
    public void isEmailFormatCorrect_ReturnTrue() {

        assertTrue(Validate.validarCorreo("dayanara@gmail.com"));
    }

    @Test
    public void isUserinBD_ReturnTrue() {

        assertTrue(Validate.validarUsuario("dayanara@gmail.com","123456"));
    }

    @Test
    public void isUserinBD_ReturnFalse() {

        assertFalse(Validate.validarUsuario("dayanra@gmail.com","123456"));
    }
}
