package facci.dayanaraburgasi.prueba_unitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    EditText editTextCorreo,getEditTextContraseña;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setViews();

    }
    private void setViews() {
        editTextCorreo = findViewById(R.id.editTextCorreo);
        getEditTextContraseña= findViewById(R.id.editTextContraseña);
        button = findViewById(R.id.button);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == button) {
            if (validateUse(editTextCorreo.getText().toString(), getEditTextContraseña.getText().toString())) {
                Toast.makeText(this, "Paso todas las validaciones", Toast.LENGTH_SHORT).show();

            } else {
                Log.e("test", "No paso las validaciones");
            }


        }

    }

    public boolean validateUse(String email, String password) {

        if (Validate.validarCampoVacio(email) || Validate.validarCampoVacio(password)) {
            Toast.makeText(this, "Los campos estan vacios", Toast.LENGTH_SHORT).show();

            return false;

        }
        if (!Validate.validarCorreo(email)) {
            Toast.makeText(this, "Correo invalido", Toast.LENGTH_SHORT).show();

            return false;
        }
        if (!Validate.validarUsuario(email, password)) {
            Toast.makeText(this, "Usuario no encontrado", Toast.LENGTH_SHORT).show();

            return false;
        }


        return true;
    }
}